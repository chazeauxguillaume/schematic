
[//]: # (0- Page de garde)

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_464c9e9104094fe213f068826d651437.png"-->
 

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px">

<div style="display: inline-block; width: 30%; vertical-align: top; border: 0px">

</div> 
    
<div style="display: inline-block; width: 20%; vertical-align: top; border: 0px">

<img src="https://minio.apps.education.fr/codimd-prod/uploads/upload_824057a2002ffbfa6e424bb832202fee.png" alt="reveal.js" style="width: 100%; margin: 0 auto 0rem auto; background: transparent; border: 0mm; "> 
</div>
    
<div style="display: inline-block; width: 48%; vertical-align: top; border: 0px">
    
<span style="color: #DA0063;font-size: 45px;text-align:right">**SCHEMATIC** </span>
    <span style="color: #333;height : 30px ;font-size: 20px;text-align:right">**des outils pour monter en compétence** </span>

</div>

</div>



<div style="display: inline-block; vertical-align: top; border: 0px;width : 100%;height : 0px ;">
  
<p style=" font-weight: bold;padding-left: 10px;background-color:#DA0063; font-size:20px; line-height: 2; color: #fff; text-align: left;width : 100%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span>  - Schematic : référentiel lycées<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFF700;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></span>
</p>

</div>

<div style="display: inline-block; width: 100%; height : 25px ; vertical-align: center; margin-bottom: 2px;">
    
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789">
<a href=#/1><span style="color: #FFF700;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span><span style="color: #FFF;"> Pilotage, impulsion et organisation </span></p></a>
     
</div>


<div style="display: inline-block; width: 100%; height : 25px ; vertical-align: center; margin-bottom: 2px">
  
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789"><a href=#/2><span style="color: #FFF700;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>
<span style="color: #FFF;">Pratiques éducatives et pédagogiques</span></a>
    
</p>
    
</div>



<div style="display: inline-block; width: 100%; height : 25px ; vertical-align: center; margin-bottom: 2px">
  
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789"><a href=#/3><span style="color: #FFF700;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>
</span>
<span style="color: #FFF;">Formation</span></a>
    
</p>
    
</div>


<div style="display: inline-block; width: 100%; height : 25px ; vertical-align: center; margin-bottom: 2px">
  
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789"><a href=#/4><span style="color: #FFF700;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>
<span style="color: #FFF;">Ressources</span></a>
    
</p>
    
</div>


<div style="display: inline-block; width: 100%; height : 20px ; vertical-align: center; margin-bottom: 2px;">

<div>
    
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789"><a href=#/5><span style="color: #FFF700;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span>
<span style="color: #FFF;">Infrastructure réseau et équipement</span></a>
    
</p>

</div>

</div>
    

---

[//]: # (1-1 Pilotage, impulsion et organisation)

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #DA0063;font-size: 45px;text-align:right">**Pilotage, impulsion et organisation**</span>
    
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></span>](#/1/1)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; vertical-align: top; border: 0px;background-color:#F5F5F5">
    
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789">
<span style="color: #fff;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span> Pilotage, impulsion et organisation - <a href=#/1/1 ><span style="color: #FFF700;"> <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a> </span> </p>    
</div>



<div style="display: inline-block; width: 95%; height: 350px; vertical-align: center; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 32%; height: 340px; vertical-align: top; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Instance numérique<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
   
<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Instance numérique : 

</p>
</div>       
    
</div>

    
<div style="display: inline-block; width: 32%; height: 340px; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Charte d'usage<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
    

<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Charte d'usage : 

</p>
</div>   
   
</div>
 
    
<div style="display: inline-block; width: 32%;height: 340px; vertical-align: top; background: #F5F5F5;">

  
<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Personnels clé<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
 
<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Personnels clé: 

</p>
</div>     
    
    
</div>
    
</div>


----

[//]: # (1-2 Pilotage, impulsion et organisation)

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #DA0063;font-size: 45px;text-align:right">**Pilotage, impulsion et organisation**</span>
    
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></span>](#/1)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></span>](#/1/2)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; vertical-align: top; border: 0px;background-color:#F5F5F5">
    
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789">
<span style="color: #fff;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span> Pilotage, impulsion et organisation  - <a href=#/1 ><span style="color: #FFF700;"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></a> </span><a href=#/1/2 ><span style="color: #FFF700;"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a> </span> </p>    
</div>



<div style="display: inline-block; width: 95%; height: 350px; vertical-align: center; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 48%; height: 340px; vertical-align: top; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> ENT<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
   
<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - ENT : 

</p>
</div>       
    
</div>

    
<div style="display: inline-block; width: 49%; height: 340px; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Communication générale<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
    

<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">
    
<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Communication générale : 

</p> 

</div>   
   
</div>
 
    

    
</div>


----

[//]: # (1-3 Pilotage, impulsion et organisation - Actions)

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #0CA789;font-size: 45px;text-align:right">**Actions possibles**</span>
<span style="color: #333;font-size: 15px;text-align:right">**Pilotage, impulsion et organisation**</span>   
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></span>](#/1/1)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; height: 450px; vertical-align: top; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 98%; height: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
Une façon simple de mettre en oeuvre une impulsion réussie est de créer un comité de pilotage interne à l'établissement afin d'amorcer les différentes actions et de fédérer un groupe autour de la thématique du numérique éducatif.
</p> 

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Informations :
</p>

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">  - Liens :
</p> 
    
<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Tutoriels :
</p> 

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Vidéos :
</p>

</p>
</div>


</div>

---

[//]: # (2-1 Pratiques éducatives et pédagogiques )

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->
 
<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #DA0063;font-size: 45px;text-align:right">**Pratiques éducatives et pédagogiques**</span>
    
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></span>](#/2/1)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; vertical-align: top; border: 0px;background-color:#F5F5F5">
    
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789">
<span style="color: #fff;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span> Pratiques éducatives et pédagogiques - <a href=#/2/1 ><span style="color: #FFF700;"> <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a> </span> </p>    
</div>



<div style="display: inline-block; width: 95%; height: 350px; vertical-align: center; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 37%; height: 340px; vertical-align: top; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Communication pédagogique<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
   
<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Communication pédagogique : 

</p>
</div>       
    
</div>

    
<div style="display: inline-block; width: 37%; height: 340px; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Compétences numériques élèves<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
    

<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Compétences numériques élèves : 

</p>
</div>   
   
</div>
 
    
<div style="display: inline-block; width: 22%;height: 340px; vertical-align: top; background: #F5F5F5;">

  
<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Inclusion<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
 
<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Inclusion: 

</p>
</div>     
    
    
</div>
    
</div>


----

[//]: # (2-2 Pratiques éducatives et pédagogiques )

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #DA0063;font-size: 45px;text-align:right">**Pratiques éducatives et pédagogiques**</span>
    
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></span>](#/1)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></span>](#/1/2)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; vertical-align: top; border: 0px;background-color:#F5F5F5">
    
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789">
<span style="color: #fff;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span> Pratiques éducatives et pédagogiques  - <a href=#/2 ><span style="color: #FFF700;"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></a> </span><a href=#/2/2 ><span style="color: #FFF700;"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a> </span> </p>    
</div>



<div style="display: inline-block; width: 95%; height: 350px; vertical-align: center; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 48%; height: 340px; vertical-align: top; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Accompagnement personnalisé<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
   
<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Accompagnement personnalisé : 

</p>
</div>       
    
</div>

    
<div style="display: inline-block; width: 49%; height: 340px; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Education aux médias et à l'information<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
    

<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">
    
<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Education aux médias et à l'information : 

</p> 

</div>   
   
</div>
 
    

    
</div>

----

[//]: # (2-3 Pratiques éducatives et pédagogiques )

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->


<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #0CA789;font-size: 45px;text-align:right">**Actions possibles**</span>
<span style="color: #333;font-size: 15px;text-align:right">**Pratiques éducatives et pédagogiques**</span>   
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></span>](#/2/1)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; height: 450px; vertical-align: top; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 98%; height: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter
</p> 

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Informations :
</p>

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">  - Liens :
</p> 
    
<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Tutoriels :
</p> 

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Vidéos :
</p>

</p>
</div>


</div>

---

[//]: # (3-1 Formation )

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #DA0063;font-size: 45px;text-align:right">**Formation**</span>
    
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></span>](#/3/1)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; vertical-align: top; border: 0px;background-color:#F5F5F5">
    
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789">
<span style="color: #fff;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span> Formation - <a href=#/3/1 ><span style="color: #FFF700;"> <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a> </span> </p>    
</div>



<div style="display: inline-block; width: 95%; height: 350px; vertical-align: center; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 48%; height: 340px; vertical-align: top; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Formation au numérique<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
   
<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Formation au numérique : 

</p>
</div>       
    
</div>

    
<div style="display: inline-block; width: 49%; height: 340px; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Compétences numériques des personnels<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
    

<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Compétences numériques des personnels : 

</p>
</div>   
   
</div>
 
    
</div>


----

[//]: # (3-1 Formation)

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #0CA789;font-size: 45px;text-align:right">**Actions possibles**</span>
<span style="color: #333;font-size: 15px;text-align:right">**Formation**</span>   
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></span>](#/3)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; height: 450px; vertical-align: top; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 98%; height: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter
</p> 

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Informations :
</p>

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">  - Liens :
</p> 
    
<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Tutoriels :
</p> 

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Vidéos :
</p>

</p>
</div>


</div>

---

[//]: # (4-1 Ressources )

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #DA0063;font-size: 45px;text-align:right">**Ressources**</span>
    
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></span>](#/4/1)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; vertical-align: top; border: 0px;background-color:#F5F5F5">
    
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789">
<span style="color: #fff;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span> Ressources - <a href=#/4/1 ><span style="color: #FFF700;"> <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a> </span> </p>    
</div>



<div style="display: inline-block; width: 95%; height: 350px; vertical-align: center; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 58%; height: 340px; vertical-align: top; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Utilisation du numérique en dehors de la classe<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
   
<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Utilisation du numérique en dehors de la classe : 

</p>
</div>       
    
</div>

    
<div style="display: inline-block; width: 39%; height: 340px; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> GAR<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
    

<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - GAR : 

</p>
</div>   
   
</div>
 
    
</div>


----

[//]: # (4-2 Ressources)

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #0CA789;font-size: 45px;text-align:right">**Actions possibles**</span>
<span style="color: #333;font-size: 15px;text-align:right">**Ressources**</span>   
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></span>](#/4)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; height: 450px; vertical-align: top; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 98%; height: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter
</p> 

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Informations :
</p>

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">  - Liens :
</p> 
    
<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Tutoriels :
</p> 

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Vidéos :
</p>

</p>
</div>


</div>

---

[//]: # (5-1 Infrastructure réseau et équipement )

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #DA0063;font-size: 45px;text-align:right">**Infrastructure réseau et équipement**</span>
    
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></span>](#/5/1)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; vertical-align: top; border: 0px;background-color:#F5F5F5">
    
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789">
<span style="color: #fff;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span> Infrastructure réseau et équipement - <a href=#/5/1 ><span style="color: #FFF700;"> <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a> </span> </p>    
</div>



<div style="display: inline-block; width: 95%; height: 350px; vertical-align: center; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 32%; height: 340px; vertical-align: top; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Accès<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
   
<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Accès : 

</p>
</div>       
    
</div>

    
<div style="display: inline-block; width: 32%; height: 340px; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Câblage<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
    

<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Câblage : 

</p>
</div>   
   
</div>
 
    
<div style="display: inline-block; width: 32%;height: 340px; vertical-align: top; background: #F5F5F5;">

  
<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Wifi<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
 
<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Wifi: 

</p>
</div>     
    
    
</div>
    
</div>

----

[//]: # (5-2 Infrastructure réseau et équipement )

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #DA0063;font-size: 45px;text-align:right">**Infrastructure réseau et équipement**</span>
    
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></span>](#/5)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></span>](#/5/2)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; vertical-align: top; border: 0px;background-color:#F5F5F5">
    
<p style="padding: 10px;font-size: 20px; line-height: 2; color: #fff; text-align: left;background-color:#0CA789">
<span style="color: #fff;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></span> Infrastructure réseau et équipement  - <a href=#/5 ><span style="color: #FFF700;"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></a> </span><a href=#/5/2 ><span style="color: #FFF700;"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i></a> </span> </p>    
</div>



<div style="display: inline-block; width: 95%; height: 350px; vertical-align: center; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 48%; height: 340px; vertical-align: top; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Vidéoprojection<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
   
<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Vidéoprojection : 

</p>
</div>       
    
</div>

    
<div style="display: inline-block; width: 49%; height: 340px; background: #F5F5F5;">

<center><p style="font-weight: bold;padding: 5px;background-color:#DA0063; font-size:16px; line-height: 2; color: #fff; text-align: center;width : 95%;border-radius: 5px;"><span style="color: #fff;"> <i class="fa fa-graduation-cap" aria-hidden="true"></i></span> Continuité pédagogique<a href=https://ww2.ac-poitiers.fr/srane/sites/srane/IMG/pdf/schematic_ref_2d.pdf><span style="color: #FFE814;"> <i class="fa fa-file-pdf-o" aria-hidden="true"></a></i></span>
</p></center>
    
    

<div style="display: inline-block; width: 100%;vertical-align: top; background: #F5F5F5;">
    
<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter - Continuité pédagogique : 

</p> 

</div>   
   
</div>
 
    

    
</div>


----

[//]: # (5-3 Infrastructure réseau et équipement )

 <!-- .slide: data-background="https://minio.apps.education.fr/codimd-prod/uploads/upload_496fd728053414640af11ec17398b9f6.png"-->

<div style="display: inline-block; width: 100%; vertical-align: top; border: 0px"> 

<div style="display: inline-block; width: 25%; vertical-align: top; border: 0px">  
</div>
    
<div style="display: inline-block; width: 50%; vertical-align: top; border: 0px"> 
    
<span style="color: #0CA789;font-size: 45px;text-align:right">**Actions possibles**</span>
<span style="color: #333;font-size: 15px;text-align:right">**Infrastructure réseau et équipement**</span>   
</div>
    
<div style="display: inline-block; width: 15%; vertical-align: top; border: 0px"> 
    
[<span style="color: #333;"><i class="fa fa-home" aria-hidden="true"></i></span>](#/0)[<span style="color: #333;"><i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i></span>](#/5/1)
    
</div>
    
</div>

<div style="display: inline-block; width: 95%; height: 450px; vertical-align: top; border: 0px; background-color:#333333">    

<div style="display: inline-block; width: 98%; height: 100%;vertical-align: top; background: #F5F5F5;">

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
A compléter 
</p> 

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Informations :
</p>

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">  - Liens :
</p> 
    
<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Tutoriels :
</p> 

<p style="font-size: 14px; line-height: 2; color: #333; text-align: left; margin-left:10px; margin-right:10px;">
- Vidéos :
</p>

</p>
</div>


</div>

---